from Bus import Bus
import random
import json,time
from pprint import pprint

def heatmap(buses):
    f = open('/opt/lampp/htdocs/te/heatmap.json', 'w')
    '''
    Get all the buses
    based on occupancy print
    '''
    data = {}
    data['location'] = []
    for bus in buses:
        for i in range(int(bus.occupancy)+1):
            data['location'].append([bus.lat, bus.long])

    json_data = json.dumps(data, indent=4)
    f.write(json_data)
    print json_data


def location(buses):
    f = open('/opt/lampp/htdocs/te/locations.json', 'w')
    '''
    Get all the buses
    based on occupancy print
    '''
    data = {}
    data['location'] = []
    for bus in buses:
        data['location'].append([bus.lat, bus.long])

    json_data = json.dumps(data, indent=4)
    f.write(json_data)
    print json_data

def generateBuses(points):
    '''
    :param points:
    :return:
    returns a list of buses
    '''
    buses = []
    for point in points:
        newbus = Bus(point[0], point[1], point[2])
        buses.append(newbus) # lat , long, occupancy

    return buses

startPoints = [(12.917440, 77.622709, 10), (12.916666, 77.599902, 20), (12.906462, 77.573235, 30)]
endPoints = [(12.932153, 77.613607), (12.917059, 77.622518), (12.906354, 77.595015)]

for i in range(0, 50):
    startPoints.append((12.977473, 77.574149, random.randint(10,50)))
    endx = random.uniform(12.8, 13.0)
    endy = random.uniform(77.2, 77.5)
    endPoints.append((endx, endy))

print startPoints
print endPoints

def movebus(bus, endPoint):
    '''
    Write a function to move the bus on a given road.
    :param bus:
    :param startPoint:
    :param endPoint:
    :return:
    '''
    
    start = (bus.lat,bus.long )
    end = (endPoint[0], endPoint[1])
    
    slope = (end[1] - start[1])/(end[0] - start[0])
    c = start[1]-slope*start[0]
    
    current_x = start[0] + 0.00001

    current_y = slope*current_x + c;
    bus.lat = current_x
    bus.long = current_y
    print bus.lat, bus.long
    return "shivam"

def shivam():
    buses = generateBuses(startPoints)
    while True:
        for i in range(0,len(buses)):
            movebus(buses[i],endPoints[i])
        heatmap(buses)
        location(buses)
        time.sleep(3)


    return "hello"


def vikasTest():
    buses = generateBuses(startPoints)
    heatmap(buses)

shivam();
